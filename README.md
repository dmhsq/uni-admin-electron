# uniCloud-admin + electron

> 基于 uni-app，uniCloud 的 admin 管理项目模板。[文档](https://uniapp.dcloud.io/uniCloud/admin)

## 开发模式

HB x 项目运行的浏览器 这一步也是为了开启云函数调试

然后执行 下面

```
npm run start
```

## 预览(预览需要打包的 electron 应用)

```
npm run dist
```

## 打安装包

```
npm run build
```

## 配置

在 package.json 修改作者信息和介绍

修改 static : 将 public 下的 logo.ico 换成自己的
