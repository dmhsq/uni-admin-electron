const {
	app,
	BrowserWindow
} = require('electron');
const isDev = require('electron-is-dev');
const path = require('path');

class AppWindow extends BrowserWindow {
	constructor(config, urlLocation) {
		const basicConfig = {
			width: 800,
			height: 600,
			webPreferences: {
				contextIsolation: false,
				nodeIntegration: true,
				enableRemoteModule: true,
				nodeIntegrationInWorker: true,
			},
			show: false,
			backgroundColor: '#efefef',
		};
		const finalConfig = {
			...basicConfig,
			...config
		};
		super(finalConfig);
		this.webContents.openDevTools();
		this.loadURL(urlLocation);
		this.once('ready-to-show', () => {
			this.show();
		});
	}
}


app.on('ready', () => {
	const mainWindowConfig = {
		width: 1440,
		height: 768,
	};
	 const urlLocation = isDev
	    ? 'http://localhost:3000'
	    : `file://${path.join(__dirname, './index.html')}`;
	mainWindow = new AppWindow(mainWindowConfig, urlLocation);
	mainWindow.on('closed', () => {
		mainWindow = null;
	});
	mainWindow.on('close', () => {
		app.quit()
	})
})
